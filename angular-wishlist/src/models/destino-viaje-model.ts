export class DestinoViaje{
  private seleted: boolean;
  constructor(public n:string, public u:string){ }

  isSlected(): boolean {
    return this.seleted;
  }

  setSelected(x) {
    this.seleted = x;
  }
}